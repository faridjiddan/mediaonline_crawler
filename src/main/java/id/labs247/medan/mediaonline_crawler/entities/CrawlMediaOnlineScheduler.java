package id.labs247.medan.mediaonline_crawler.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "crwl_media_online_scheduler")
public class CrawlMediaOnlineScheduler {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "media_online_scheduler_id")
    private Long mediaOnlineSchedulerId;

    @Column(name = "original_domain")
    private String originalDomain;

    @Column(name = "landing_url")
    private String landingUrl;

    @Column(name = "last_scheduled")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastScheduled;

    @Column(name = "schedule_minutes")
    private Integer scheduleMinutes;

    @Column(name = "created_by")
    private Integer createdBy;

    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;

    @Column(name = "status")
    private Integer status;

    @Column(name = "media_logo")
    private String mediaLogo;

    @Column(name = "url_select")
    private String urlSelect;

    @Column(name = "content_select")
    private String contentSelect;

    @Column(name = "parse_auto")
    private Integer parseAuto;

    public Long getMediaOnlineSchedulerId() {
        return mediaOnlineSchedulerId;
    }

    public void setMediaOnlineSchedulerId(Long mediaOnlineSchedulerId) {
        this.mediaOnlineSchedulerId = mediaOnlineSchedulerId;
    }

    public String getOriginalDomain() {
        return originalDomain;
    }

    public void setOriginalDomain(String originalDomain) {
        this.originalDomain = originalDomain;
    }

    public String getLandingUrl() {
        return landingUrl;
    }

    public void setLandingUrl(String landingUrl) {
        this.landingUrl = landingUrl;
    }

    public Date getLastScheduled() {
        return lastScheduled;
    }

    public void setLastScheduled(Date lastScheduled) {
        this.lastScheduled = lastScheduled;
    }

    public Integer getScheduleMinutes() {
        return scheduleMinutes;
    }

    public void setScheduleMinutes(Integer scheduleMinutes) {
        this.scheduleMinutes = scheduleMinutes;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMediaLogo() {
        return mediaLogo;
    }

    public void setMediaLogo(String mediaLogo) {
        this.mediaLogo = mediaLogo;
    }

    public String getUrlSelect() {
        return urlSelect;
    }

    public void setUrlSelect(String urlSelect) {
        this.urlSelect = urlSelect;
    }

    public String getContentSelect() {
        return contentSelect;
    }

    public void setContentSelect(String contentSelect) {
        this.contentSelect = contentSelect;
    }

    public Integer getParseAuto() {
        return parseAuto;
    }

    public void setParseAuto(Integer parseAuto) {
        this.parseAuto = parseAuto;
    }

    

}
