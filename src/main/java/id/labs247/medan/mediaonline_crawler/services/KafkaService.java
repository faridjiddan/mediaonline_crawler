package id.labs247.medan.mediaonline_crawler.services;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("KafkaService")
public class KafkaService {

    private static final String KAFKA_TOPIC_CONTENT = "news-topic-content";
    // private static final String KAFKA_SERVERS = "localhost:9092";
    private static final long TIMEOUT_MILLIS = 300000;

    @Value("${spring.kafka.bootstrap-servers}")
    private String KAFKA_SERVERS;

    Producer<String, String> kafkaProducer;
    
    public void sendUrlToKafka(String object, String topic) throws Exception {
        try {
            // Configure Kafka producer properties
            Properties properties = new Properties();
            properties.put("bootstrap.servers", KAFKA_SERVERS);
            properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
            properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

            // Create Kafka producer
            kafkaProducer = new KafkaProducer<>(properties);

            // Send news to Kafka
            System.out.println("Sending News URL to Kafka . . .");
            kafkaProducer.send(new ProducerRecord<>(topic, object));
        } catch (Exception e) {
            kafkaProducer.close();
            throw new Exception("Failed send to Kafka");
        }

    }

    public void sendContentToKafka(String object) throws Exception {
        try {
            // Configure Kafka producer properties
            Properties properties = new Properties();
            properties.put("bootstrap.servers", KAFKA_SERVERS);
            properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
            properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

            // Create Kafka producer
            kafkaProducer = new KafkaProducer<>(properties);

            // Send news to Kafka
            System.out.println("Sending News Content to Kafka . . .");
            kafkaProducer.send(new ProducerRecord<>(KAFKA_TOPIC_CONTENT, object));
        } catch (Exception e) {
            kafkaProducer.close();
            throw new Exception("Failed send to Kafka");
        }

    }

    public List<ConsumerRecord<String, String>> subscribeFromKafka(String topic) throws Exception {

        List<ConsumerRecord<String, String>> buffer = new ArrayList<>();
    
        Properties props = new Properties();
        props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_SERVERS);
        props.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "test");
        props.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
        props.setProperty("auto.commit.interval.ms", "1000");
        props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
    
        try (KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props)) {
            System.out.println("Receiving from Kafka . . .");
            consumer.subscribe(Arrays.asList(topic));
            // Set a timeout for the loop, or you can use another mechanism to break out of the loop.
            long endTimeMillis = System.currentTimeMillis() + TIMEOUT_MILLIS;
    
            while (System.currentTimeMillis() < endTimeMillis) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
                for (ConsumerRecord<String, String> record : records) {
                    buffer.add(record);
                }
            }

            consumer.close();
            return buffer;
        } catch (Exception e) {
            throw new Exception("Failed to subscribe from Kafka", e);
        }

    }

    
    public List<String> parsingKafka(List<ConsumerRecord<String, String>> records) {
        List<String> result = new ArrayList<>();

        for(ConsumerRecord<String, String> value : records) {
            result.add(value.value());
        }
        return result;
    }

    public void closeKafka() {
        // Close Kafka
        kafkaProducer.close();
    }
}
