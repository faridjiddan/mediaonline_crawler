package id.labs247.medan.mediaonline_crawler.services;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import id.labs247.medan.mediaonline_crawler.entities.CrawlMedia;
import id.labs247.medan.mediaonline_crawler.repositories.CrawlMediaRepository;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


@Service("NewsScrappingService")
public class NewsScrappingService {

    private final String userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36";

    @Autowired
    KafkaService kafkaService;

    @Autowired
    CrawlMediaRepository crawlMediaRepository;
    
    public void kompasIndex() throws Exception {

        try {
            String domain = "kompas.com";
            CrawlMedia crawlMedia = crawlMediaRepository.getByDomain(domain);
            String baseUrl = crawlMedia.getLandingUrl();
            int depth = 0;
            String topic = "news-topic-kompas";
            String date = dateFormatter("yyyy-MM-dd");

            for(int i = 1; i <= 3; i++) {
                String url = baseUrl+date+"/"+i;
                Document document = Jsoup.connect(url).userAgent(userAgent).get();
                System.out.println("Processing Kompas.com: Parsing Index Page " + i);
                Elements articles = document.select(".article__list.clearfix");
                for(Element article : articles) {     
                
                    Element titleElement = article.selectFirst(".article__title a");
                    String link = titleElement.attr("href");
                    if(depth <= 4) {
                        JSONObject jsonUrl = createJsonKafkaUrl(link, url, domain, domain, depth,".article__title a[href]", "");
                        kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                    }
                    
                }
            }


            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    kompasScrapper(newLandingUrl, newUrl, newDepth);
                }
            }

                
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Kompas.com");
        }
    }

    public void kompasScrapper(String landingUrl, String url, int depth) throws Exception {
        try {
            System.out.println("Processing Kompas.com: Parsing Content and Related News");
            Document document = Jsoup.connect(url+"?page=all").userAgent(userAgent).get();
            Elements paragraphs = document.select("div.read__content > div.clearfix > p");

            String topic = "news-topic-kompas";
            String domain = "kompas.com";

            depth += 1; 

            // Parsing content
            String content = "";
            
            for(Element paragraph : paragraphs) {
                if(!paragraph.text().contains("Baca juga")) {
                    content += paragraph.text(); 
                }
            }

            JSONObject jsonContent = createJsonKafkaContent(url, domain, content);

            if(content.length()!=0 && depth <= 4) {
                kafkaService.sendContentToKafka(jsonContent.toString());
            }

            
            // Parsing link inside content
            for (Element link : paragraphs.select("a")) {
                String href = link.attr("href");
                if(!href.contains("tag") && depth <= 4) {
                    JSONObject jsonUrl = createJsonKafkaUrl(href, url, domain, domain, depth,"div.read__content > div.clearfix > p a[href]", "div.read__content > div.clearfix > p");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic); 
                }

            }


            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));
            
            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    kompasScrapper(newLandingUrl, newUrl, newDepth);
                }
            }

            
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Kompas.com");
        }

    }

    public void liputan6Index() throws Exception {
        try {
            String domain = "liputan6.com";
            CrawlMedia crawlMedia = crawlMediaRepository.getByDomain(domain);
            String baseUrl = crawlMedia.getLandingUrl();
            int depth = 0;
            String topic = "news-topic-liputan6";
            String date = dateFormatter("yyyy/MM/dd");
            
            for(int i = 1; i <= 3; i++) {
                String url = baseUrl+date+"?page="+i;
                Document document = Jsoup.connect(url).userAgent(userAgent).get();
                System.out.println("Processing Liputan6: Parsing Index Page " + i);
                Elements articles = document.select("div.articles--list.articles--list_rows article");
                if(articles.toString().length()!=0) {
                    for (Element article : articles) {
                        String link = article.select("h4.articles--rows--item__title a.articles--rows--item__title-link").attr("href");
                        if(depth <= 4) {   
                            JSONObject jsonUrl = createJsonKafkaUrl(link, url, domain, domain, depth,"h4.articles--rows--item__title a.articles--rows--item__title-link href", "");
                            kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                        }
                    }
                }
            }

            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    liputan6Scrapper(newLandingUrl, newUrl, newDepth);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Liputan6");
        }
    }

    public void liputan6Scrapper(String landingUrl, String url, int depth) throws Exception {
        try {
            String domain = "liputan6.com";
            String topic = "news-topic-liputan6";
            System.out.println("Processing Liputan6.com: Parsing Content and Related News");
            Document document = Jsoup.connect(url+"?page=all").userAgent(userAgent).get();
            Elements elements = document.select("div.article-content-body__item-content");

            Elements paragraphs = elements.select("p:not(:contains(Advertisement)):not(:contains(BACA JUGA:)):not(:contains(Baca Juga)):not(:contains(Reporter:)):not(:contains(Sumber:))");

            depth += 1; 

            // Parsing content
            String content = "";
            
            for(Element paragraph : paragraphs) {
                content += paragraph.text(); 
            }

            JSONObject jsonContent = createJsonKafkaContent(url, domain, content);
            if(content.length()!=0 && depth <= 4) {
                kafkaService.sendContentToKafka(jsonContent.toString());
            }

            // Parsing link inside content
            Elements links = document.select(".baca-juga-collections__detail a");
            for (Element link : links) {
                String href = link.attr("href");
                if(depth <= 4 && href.contains("news") && !href.contains("video")) {
                    JSONObject jsonUrl = createJsonKafkaUrl(href, url, domain, domain, depth,".baca-juga-collections__detail a", "");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }
            }

            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    liputan6Scrapper(newLandingUrl, newUrl, newDepth);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Liputan6");
        }        
    }

    public void detikIndex() throws Exception {
        try {
            String domain = "detik.com";
            CrawlMedia crawlMedia = crawlMediaRepository.getByDomain(domain);
            String baseUrl = crawlMedia.getLandingUrl();
            int depth = 0;
            String topic = "news-topic-detik";
            String date = dateFormatter("MM/dd/yyyy");

            for(int i = 1; i <=3; i++) {
                String url = baseUrl+i+"?date="+date;
                Document document = Jsoup.connect(url).userAgent(userAgent).get();
                System.out.println("Processing Detik.com: Parsing Index Page " + i);
                Elements articles = document.select(".grid-row.list-content article.list-content__item h3.media__title a");

                for (Element article : articles) {
                    String link = article.attr("href");
                    if(depth <= 4 && !link.contains("20.detik.com") && !link.contains("foto-news") && !link.contains("detiktv")) {
                        JSONObject jsonUrl = createJsonKafkaUrl(link, url, domain, domain, depth, "h3.media__title a[href]", ".grid-row.list-content article.list-content__item");
                        kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                    }
                    
                }
            }

            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    detikScrapper(newLandingUrl, newUrl, newDepth);
                }
            }          

            
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Detik.com");
        }
    }

    public void detikScrapper(String landingUrl, String url, int depth) throws Exception {
        try {
            System.out.println("Processing Detik.com: Parsing Content and Related News");
            Document document = Jsoup.connect(url+"?single=1").userAgent(userAgent).get();
            Elements elements = document.select("div.detail__body-text.itp_bodycontent");
            Elements paragraphs = elements.select("p:not(:contains(Baca juga)):not(:contains(Simak Video)):not(:contains(ADVERTISEMENT)):not(:contains(SCROLL TO CONTINUE WITH CONTENT)):not(:contains([Gambas:Video 20detik])):not(:contains(Simak selengkapnya di sini.))");
            String content = "";
            String topic = "news-topic-detik";

            String domain = "detik.com";
            for(Element paragraph : paragraphs) {
                content += paragraph.text();
            }
            
            depth += 1; 


            JSONObject jsonContent = createJsonKafkaContent(url, domain, content);

            if(content.length()!=0 && depth <= 4) {
                kafkaService.sendContentToKafka(jsonContent.toString());
            }

            // Parsing link inside content
            Elements bacaJugaLinks = document.select("div.lihatjg");
            for (Element link : bacaJugaLinks.select("a")) {
                String href = link.attr("href");
                if(depth <= 4) {
                    JSONObject jsonUrl = createJsonKafkaUrl(href, url, domain, domain, depth, "div.lihatjg", "");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }

            }

            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    detikScrapper(newLandingUrl, newUrl, newDepth);
                }
            } 

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Detik.com");
        }
    }

    public void tribunnewsIndex() throws Exception{
        try {
            String domain = "tribunnews.com";
            CrawlMedia crawlMedia = crawlMediaRepository.getByDomain(domain);
            String baseUrl = crawlMedia.getLandingUrl();
            int depth = 0;
            String topic = "news-topic-tribunnews";
            String date = dateFormatter("yyyy-MM-dd");

            for(int i = 1; i <= 3; i++) {
                String url = baseUrl+"?date="+date+"&page="+i;
                Document document = Jsoup.connect(url).userAgent(userAgent).get();
            
                System.out.println("Processing Tribunnews: Parsing Index Page " + i);
                Elements articles = document.select("ul.lsi li");

                for (Element article : articles) {
                    String link = article.select("li.ptb15 h3.f16.fbo a").attr("href");
                    if(link.length()!=0 && depth <= 4) {
                        JSONObject jsonUrl = createJsonKafkaUrl(link, url, domain, domain, depth, "li.ptb15 h3.f16.fbo a[href]", "ul.lsi li");
                        kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                    }

                } 

            }

            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    tribunnewsScrapper(newLandingUrl, newUrl, newDepth);
                }
            } 


           
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Tribunnews.com");
        }
    }

    public void tribunnewsScrapper(String landingUrl, String url, int depth) throws Exception {
        try {
            System.out.println("Processing Tribunnews: Parsing Content and Related News");
            Document document = Jsoup.connect(url+"?page=all").userAgent(userAgent).get();
            Elements elements = document.select(".side-article.txt-article.multi-fontsize");
            Elements paragraphs = elements.select("p:not(:contains(Baca juga:))");
            String content = "";
            String topic = "news-topic-tribunnews";
            String domain = "tribunnews.com";
            for(Element paragraph : paragraphs) {
                content += paragraph.text();
            }
            
            depth += 1; 

            JSONObject jsonContent = createJsonKafkaContent(url, domain, content);

            if(content.length()!=0 && depth <= 4) {
                kafkaService.sendContentToKafka(jsonContent.toString());
            }

            // Parsing link inside content
            for (Element link : elements.select("p.baca a")) {
                String href = link.attr("href");
                if(depth <= 4) {
                    JSONObject jsonUrl = createJsonKafkaUrl(href, url, domain, domain, depth, "p.baca a", "");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }

            }


            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    tribunnewsScrapper(newLandingUrl, newUrl, newDepth);
                }
            } 

            
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Tribunnews.com");
        }
    }

    public void cnnIndonesiaIndex() throws Exception {
        try {
            String domain = "cnnindonesia.com";
            CrawlMedia crawlMedia = crawlMediaRepository.getByDomain(domain);
            String baseUrl = crawlMedia.getLandingUrl()+"/2/";
            int depth = 0;
            String topic = "news-topic-cnn";
            String date = dateFormatter("yyyy/MM/dd");

            for (int i = 1; i <= 3; i++) {
                String url = baseUrl+i+"?date="+date;
                Document document = Jsoup.connect(url).userAgent(userAgent).get();
                System.out.println("Processing CNN Indonesia: Parsing Index Page");

                Elements articles = document.select(".flex.gap-4");
                for(Element article : articles) {
                    Element linkElement = article.selectFirst("article a");
                    if(linkElement!=null && depth <= 4) {
                        String link = linkElement.attr("href");
                        Document doc = Jsoup.connect(link).userAgent(userAgent).get();
                        Elements elementLink = doc.select("div.detail-text.text-cnn_black.text-sm.grow.min-w-0");
                        if(elementLink.text().length()!=0) {
                            JSONObject jsonUrl = createJsonKafkaUrl(link, url, domain, domain, depth, "div.detail-text.text-cnn_black.text-sm.grow.min-w-0", "");
                            kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                        }
                        
                    }
                }
            }

            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    cnnIndonesiaScrapper(newLandingUrl, newUrl, newDepth);
                }
            } 


        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping CNN Indonesia");
        }
    }

    public void cnnIndonesiaScrapper(String landingUrl, String url, int depth) throws Exception {
        try {
            System.out.println("Processing CNN Indonesia: Parsing Content and Related News");
            Document document = Jsoup.connect(url).userAgent(userAgent).get();
            Elements elements = document.select("div.detail-text.text-cnn_black.text-sm.grow.min-w-0");
            Elements paragraphs = elements.select("p:not(:contains(Lihat Juga)):not(:contains(ADVERTISEMENT)):not(:contains([Gambas:Video CNN])):not(:contains(SCROLL TO CONTINUE WITH CONTENT)):not(:contains(Baca selengkapnya di sini)):not(:contains(Bersambung ke halaman berikutnya...))");
            String topic = "news-topic-cnn";
            String domain = "cnnindonesia.com";
            depth += 1;

            String content = "";
            for(Element paragraph : paragraphs) {
                content += paragraph.text();
            }

            // For handling pagination of news
            content += cnnPaginationNews(elements, content);
            
            JSONObject jsonContent = createJsonKafkaContent(url, domain, content);

            if(content.length()!=0 && depth <= 4) {
                kafkaService.sendContentToKafka(jsonContent.toString());
            }

            // Parsing link inside content
            for (Element link : elements.select("div.lihatjg-subtitle a")) {
                String href = link.attr("href");
                if(depth <= 4) {
                    JSONObject jsonUrl = createJsonKafkaUrl(href, url, domain, domain, depth, "div.lihatjg-subtitle a", "");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }

            }

            // For handling pagination related news
            cnnPaginationLink(elements, url, domain, depth, topic);


            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    cnnIndonesiaScrapper(newLandingUrl, newUrl, newDepth);
                }
            } 


        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping CNN Indonesia");
        }
    }

    public void mediaIndonesiaIndex() throws Exception {
        try {
            String domain = "mediaindonesia.com";
            CrawlMedia crawlMedia = crawlMediaRepository.getByDomain(domain);
            String baseUrl = crawlMedia.getLandingUrl();
            int depth = 0;
            String topic = "news-topic-mediaindonesia";
            

            for(int i = 0; i <3; i++) {
                String url = baseUrl+(i*20);
                Document document = Jsoup.connect(url).userAgent(userAgent).get();
                System.out.println("Processing Media Indonesia: Parsing Index Page " + i);
                Elements articles = document.select("ul.list-3 li");

                for (Element article : articles) {

                    String link = article.select("div.text a").attr("href");
                    if(link.length()!=0 && depth <= 4) {
                        JSONObject jsonUrl = createJsonKafkaUrl(link, url, domain, domain, depth, "ul.list-3 li div.text a[href]", "");
                        kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                    }
                    
                }

            }

            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    mediaIndonesiaScrapper(newLandingUrl, newUrl, newDepth);
                }
            } 
            

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping MediaIndonesia.com");
        }           
    }

    public void mediaIndonesiaScrapper(String landingUrl, String url, int depth) throws Exception {
        try {
            System.out.println("Processing Media Indonesia: Parsing Content and Related News");
            Document document = Jsoup.connect(url).userAgent(userAgent).get();
            Elements elements = document.select("div.article");
            Elements paragraphs = elements.select("p:not(:contains(Baca juga))");
            String content = "";
            String topic = "news-topic-mediaindonesia";
            String domain = "mediaindonesia.com";
            for(Element paragraph : paragraphs) {
                content += paragraph.text();
            }
            
            depth += 1; 

            JSONObject jsonContent = createJsonKafkaContent(url, domain, content);

            if(content.length()!=0 && depth <= 4) {
                kafkaService.sendContentToKafka(jsonContent.toString());
            }
            
            // Parsing link inside content
            Elements linkElements = document.select("ul.list-3 div.text a");
            for (Element linkElement : linkElements) {
                String link = linkElement.attr("href");
                if(link.length()!=0 && depth <= 4) {
                    JSONObject jsonUrl = createJsonKafkaUrl(link, url, domain, domain, depth, "ul.list-3 li div.text a[href]", "");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }
            }


            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    mediaIndonesiaScrapper(newLandingUrl, newUrl, newDepth);
                }
            } 
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping MediaIndonesia.com");
        }           
    }


    public void tempoIndex() throws Exception {
        try {
            String domain = "tempo.co";
            int depth = 0;
            String topic = "news-topik-tempo";
            CrawlMedia crawlMedia = crawlMediaRepository.getByDomain(domain);
            String date = dateFormatter("yyyy-MM-dd");
            String baseUrl = crawlMedia.getLandingUrl()+date+"/";
            Document document = Jsoup.connect(baseUrl).userAgent(userAgent).get();
            System.out.println("Processing Tempo.co: Parsing Index Page");
            Elements elements = document.select("main.main-left div.card-box.ft240.margin-bottom-sm article.text-card a");

            for(Element element : elements) {
                String link = element.attr("href");
                if(link.length()!=0 && depth <= 4) {
                    JSONObject jsonUrl = createJsonKafkaUrl(link, baseUrl, domain, domain, depth, "main.main-left div.card-box.ft240.margin-bottom-sm article.text-card a[href]", "");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }
            }

            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    tempoScrapper(newLandingUrl, newUrl, newDepth);
                }
            } 

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Tempo.co");
        }
    }

    public void tempoScrapper(String landingUrl, String url, int depth) throws Exception {
        try {
            System.out.println("Processing Tempo.co: Parsing Content and Related News");
            Document document = Jsoup.connect(url).userAgent(userAgent).get();
            Elements elements = document.select("div.detail-konten");
            Elements paragraphs = elements.select("p:not(:contains(Baca Juga)):not(:contains(Pilihan Editor))");
            String content = "";
            String topic = "news-topic-tempo";
            String domain = "tempo.co";

            for(Element paragraph : paragraphs) {
                content += paragraph.text();
            }
            
            depth += 1; 

            JSONObject jsonContent = createJsonKafkaContent(url, domain, content);

            if(content.length()!=0 && depth <= 4) {
                kafkaService.sendContentToKafka(jsonContent.toString());
            }
            
            // Parsing link inside content
            Elements linkElements = elements.select("div.bacajuga a");
            for (Element linkElement : linkElements) {
                String link = linkElement.attr("href");
                if(link.length()!=0 && depth <= 4) {
                    JSONObject jsonUrl = createJsonKafkaUrl(link, url, domain, domain, depth, "ul.list-3 li div.text a[href]", "");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }
            }


            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    tempoScrapper(newLandingUrl, newUrl, newDepth);
                }
            }             
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Tempo.co");
        }
    }

    public void beritasatuIndex() throws Exception {
        try {
            String domain = "beritasatu.com";
            CrawlMedia crawlMedia = crawlMediaRepository.getByDomain(domain);
            String baseUrl = crawlMedia.getLandingUrl();
            int depth = 0;
            String topic = "news-topic-beritasatu";
            System.out.println("Processing Beritasatu.com: Parsing Index Page");
            String date = dateFormatter("yyyy-MM-dd");

            for (int i = 1; i <= 3; i++) {
                String url = baseUrl+i+"?date="+date;
                Document document = Jsoup.connect(url).userAgent(userAgent).get();
                Elements elements = document.select("div.col");
                if(!elements.text().contains("Data Tidak Ditemukan")) {
                    for (Element element : elements.select("a.stretched-link")) {
                        String href = element.attr("href");
                        if(depth <=4 && !href.contains("https://www.beritasatu.com/multimedia/")) {
                            JSONObject jsonUrl = createJsonKafkaUrl(href, url, domain, domain, depth, "div.col a.stretched-link", "");
                            kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                        }
                    }
                }
            }


            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    beritasatuScrapper(newLandingUrl, newUrl, newDepth);
                }
            } 

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Beritasatu");
        }
    }

    public void beritasatuScrapper(String landingUrl, String url, int depth) throws Exception {
        try {
            System.out.println("Processing Beritasatu.com: Parsing Content and Related News");
            Document document = Jsoup.connect(url).userAgent(userAgent).get();
            Elements elements = document.select("div.col.b1-article.body-content");
            Elements paragraphs = elements.select("p:not(:contains(BACA JUGA)):not(:contains(ADVERTISEMENT)):not(div.mt-4)");
            String content = "";
            String topic = "news-topic-beritasatu";
            String domain = "beritasatu.com";

            for(Element paragraph : paragraphs) {
                content += paragraph.text();
            }
            
            depth += 1; 

            JSONObject jsonContent = createJsonKafkaContent(url, domain, content);

            if(content.length()!=0 && depth <= 4) {
                kafkaService.sendContentToKafka(jsonContent.toString());
            }

            // Parsing link inside content
            Elements linkElements = document.select("a.text-dark.stretched-link");
            for (Element linkElement : linkElements) {
                String link = linkElement.attr("href");
                if(link.length()!=0 && depth <= 4) {
                    JSONObject jsonUrl = createJsonKafkaUrl(link, url, domain, domain, depth, "ul.list-3 li div.text a[href]", "");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }
            }


            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    beritasatuScrapper(newLandingUrl, newUrl, newDepth);
                }
            }   
                      
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Beritasatu");
        }
    }

    public void okezoneIndex() throws Exception {
        try {
            String domain = "okezone.com";
            CrawlMedia crawlMedia = crawlMediaRepository.getByDomain(domain);
            String baseUrl = crawlMedia.getLandingUrl();
            int depth = 0;
            String topic = "news-topic-okezone";
            System.out.println("Processing Okezone.com: Parsing Index Page");
            String date = dateFormatter("yyyy/MM/dd");

            for (int i = 0; i < 3; i++) {
                String url = baseUrl+date+"/"+(i*10);
                Document document = Jsoup.connect(url).userAgent(userAgent).get();
                Elements elements = document.select("div.news-content h4.f17 a");
                for (Element element : elements) {
                    String href = element.attr("href");
                    if(depth <=4) {
                        JSONObject jsonUrl = createJsonKafkaUrl(href, url, domain, domain, depth, "div.news-content h4.f17 a", "");
                        kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                    }
                }
            }


            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    okezoneScrapper(newLandingUrl, newUrl, newDepth);
                }
            } 

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Okezone");
        }
    }

    public void okezoneScrapper(String landingUrl, String url, int depth) throws Exception {
        try {
            System.out.println("Processing Okezone.com: Parsing Content and Related News");
            Document document = Jsoup.connect(url).userAgent(userAgent).get();
            Elements elements = document.select("#contentx");
            Elements paragraphs = elements.select("p:not(:contains(BACA JUGA)):not(div.detail-tag.newtag):not(div.paging.newpaging.clearfix)");
            String content = "";
            String topic = "news-topic-okezone";
            String domain = "okezone.com";

            for(Element paragraph : paragraphs) {
                if(!paragraph.text().contains("Follow Berita Okezone di") && !paragraph.text().contains("Dapatkan berita up to date dengan semua berita terkini dari Okezone"))
                    content += paragraph.text();
            }
            
            depth += 1; 

            JSONObject jsonContent = createJsonKafkaContent(url, domain, content);

            if(content.length()!=0 && depth <= 4) {
                kafkaService.sendContentToKafka(jsonContent.toString());
            }

            // Parsing link inside content
            Elements linkElements = document.select("div.description-baca-juga a");
            for (Element linkElement : linkElements) {
                String link = linkElement.attr("href");
                if(link.length()!=0 && depth <= 4) {
                    JSONObject jsonUrl = createJsonKafkaUrl(link, url, domain, domain, depth, "div.description-baca-juga a[href]", "");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }
            }


            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    okezoneScrapper(newLandingUrl, newUrl, newDepth);
                }
            }   
                      
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Okezone");
        }
    }


    public void tirtoIndex() throws Exception {
        try {
            String domain = "tirto.id";
            CrawlMedia crawlMedia = crawlMediaRepository.getByDomain(domain);
            String baseUrl = crawlMedia.getLandingUrl();
            int depth = 0;
            String topic = "news-topic-tirto";
            System.out.println("Processing Tirto.id: Parsing Index Page");

            List<String> arrayLink = new ArrayList<>();

            // for (int i = 1; i <= 3; i++) {
                String url = baseUrl;
                Document document = Jsoup.connect(url).userAgent(userAgent).get();
                Elements elements = document.select("script[type=text/javascript]");
                for (Element script : elements) {
                    // Get the text content of the script tag
                    String scriptText = script.html().replace("window.__NUXT__=","").replace("};", "}");

                    // Extract JSON data from the script text
                    try {
                        JSONObject jsonData = new JSONObject(scriptText);
                        JSONArray dataList = jsonData.getJSONArray("data");
                        // Iterate through the JSONArray
                        for (int i = 0; i < dataList.length(); i++) {
                            // Get the JSONObject at the current index
                            JSONObject jsonObject = dataList.getJSONObject(i);

                            // Check if the "listupdate" field exists in the JSONObject
                            if (jsonObject.has("listupdate")) {
                                // Extract the "listupdate" field as a JSONArray
                                JSONArray listUpdateArray = jsonObject.getJSONArray("listupdate");
                                for(int j = 0; j < listUpdateArray.length(); j++) {
                                    JSONObject jsonExtracted = listUpdateArray.getJSONObject(j);
                                    String linkToNews = jsonExtracted.getString("articleUrlBaru");
                                    if(linkToNews.length()!=0)
                                        arrayLink.add("https://tirto.id"+linkToNews);
                                }
                            }
                        }  
                    } catch (Exception e) {
                        // Handle JSON parsing errors
                        e.printStackTrace();
                        System.err.println("Error parsing JSON: " + e.getMessage());
                    }
                }
            // }

            for(String link : arrayLink) {
                if(depth <= 4) {
                    JSONObject jsonUrl = createJsonKafkaUrl(link, url, domain, domain, depth, "", "");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }
                    
            }

            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    tirtoScrapper(newLandingUrl, newUrl, newDepth);
                }
            } 

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Tirto");
        }
    }

    public void tirtoScrapper(String landingUrl, String url, int depth) throws Exception {
        try {
            System.out.println("Processing tirto.id: Parsing Content and Related News");
            Document document = Jsoup.connect(url).userAgent(userAgent).get();
            Elements elements = document.select("script[type=text/javascript]");
            String isiBerita = "";
            List<String> linkList = new ArrayList<>();
            depth += 1;
            String topic = "news-topic-tirto";
            String domain = "tirto.id";

            for(Element element : elements) {
                String scriptText = element.html().replace("window.__NUXT__=","").replace("};", "}");
                try {
                    JSONObject jsonData = new JSONObject(scriptText);
                    JSONArray dataList = jsonData.getJSONArray("data");
                    // Iterate through the JSONArray
                    for (int i = 0; i < dataList.length(); i++) {
                        // Get the JSONObject at the current index
                        JSONObject jsonObject = dataList.getJSONObject(i);
                        JSONObject jsonArticle = jsonObject.getJSONObject("article");

                        // Parsing content
                        JSONArray content = jsonArticle.getJSONArray("isi_split");
                        for (int j = 0; j < content.length(); j++) {
                            if(!content.get(j).toString().contains("script") && !content.get(j).toString().contains("div class=")) {
                                isiBerita += content.get(j);
                            }

                            if(content.get(j).toString().contains("div class=")) {
                                Elements linkElements = Jsoup.parse(content.get(j).toString()).select("div.baca-holder.bold ul li a");
                                for (Element linkElement : linkElements) {
                                    String href = linkElement.attr("href");
                                    linkList.add(href);
                                }
                            }

                        }

                    }  
                } catch (Exception e) {
                    e.printStackTrace();
                    System.err.println("Error parsing JSON: " + e.getMessage());
                }

            }

            JSONObject jsonContent = createJsonKafkaContent(url, domain, isiBerita);

            if(isiBerita.length()!=0 && depth <= 4) {
                kafkaService.sendContentToKafka(jsonContent.toString());
            }

            // Parsing link inside content
            for (String linkNews : linkList) {
                if(linkNews.length()!=0 && depth <= 4) {
                    JSONObject jsonUrl = createJsonKafkaUrl(linkNews, url, domain, domain, depth, "div.baca-holder.bold ul li a[href]", "");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }
            }


            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    tirtoScrapper(newLandingUrl, newUrl, newDepth);
                }
            }   
                      
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Tirto");
        }
    }

    public void kumparanIndex() throws Exception {
        try {
            String domain ="kumparan.com";
            CrawlMedia crawlMedia = crawlMediaRepository.getByDomain(domain);
            int depth = 0;
            String topic = "news-topic-kumparan";
            String baseUrl = crawlMedia.getLandingUrl();
            System.out.println("Processing kumparan.com: Parsing Index Page");

            List<String> linkList = new ArrayList<>();

            OkHttpClient client = new OkHttpClient();

            String url = "https://graphql-v4.kumparan.com/query?deduplicate=1";
            for (int j = 1; j <= 3; j++) {
                String jsonBody = "[{\"operationName\":\"FindStoryFeedByChannelSlug\",\"variables\":{\"channelSlug\":\"news\",\"cursor\":\"" + j + "\",\"size\":10,\"cursorType\":\"PAGE\",\"userAliasID\":\"53273844-33bb-4fa0-b20a-eacd9a0892d6\"},\"query\":\"query FindStoryFeedByChannelSlug($channelSlug: String!, $userAliasID: ID, $size: Int!, $cursor: String!, $cursorType: CursorType!) {\\n  FindStoryFeedByChannelSlug(\\n    channelSlug: $channelSlug\\n    userAliasID: $userAliasID\\n    cursorType: $cursorType\\n    size: $size\\n    cursor: $cursor\\n  ) {\\n    edges {\\n      ...CompactStory\\n      __typename\\n    }\\n    cursorInfo {\\n      ...CursorInfo\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\\nfragment CompactStory on Story {\\n  __typename\\n  id\\n  authorID\\n  title\\n  customTitle\\n  createdAt\\n  leadText\\n  publishedAt\\n  isAgeRestrictedContent\\n  slug\\n  isStickyStory\\n  isShowOnWeb\\n  isShowOnApp\\n  isDisableComment\\n  isDisableLike\\n  isDisableShare\\n  isSnackable\\n  metaDescription\\n  readTimeInMinutes\\n  storyAddOns {\\n    ...StoryAddOn\\n    __typename\\n  }\\n  author {\\n    ...SimpleUser\\n    __typename\\n  }\\n  publisher {\\n    ...SimplePublisher\\n    __typename\\n  }\\n  leadMedia {\\n    ...Media\\n    __typename\\n  }\\n  headline {\\n    ...Headline\\n    __typename\\n  }\\n  statistic {\\n    ...StoryStatistic\\n    __typename\\n  }\\n  readEligibility\\n  productInfo {\\n    ...Product\\n    __typename\\n  }\\n  isPrivate\\n  collection {\\n    ...CompactCollection\\n    __typename\\n  }\\n  internalTags\\n  deletedAt\\n  deletionInfo {\\n    __typename\\n    deleterType\\n    deletedBy {\\n      __typename\\n      id\\n      name\\n    }\\n  }\\n}\\n\\nfragment StoryAddOn on StoryAddOn {\\n  object {\\n    __typename\\n    ... on Polling {\\n      ...Polling\\n      __typename\\n    }\\n    ... on Gallery {\\n      ...Gallery\\n      __typename\\n    }\\n    ... on Form {\\n      ...Form\\n      __typename\\n    }\\n    ... on Recipe {\\n      ...Recipe\\n      __typename\\n    }\\n    ... on LiveBlog {\\n      ...LiveBlogAddOnDetail\\n      __typename\\n    }\\n  }\\n  addOnType\\n  __typename\\n}\\n\\nfragment Polling on Polling {\\n  __typename\\n  id\\n  name\\n  description\\n  mediaID\\n  startsAt\\n  endsAt\\n  questions {\\n    ...Question\\n    __typename\\n  }\\n}\\n\\nfragment Question on Question {\\n  id\\n  pollingID\\n  detail\\n  position\\n  choices {\\n    ...Choice\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment Choice on Choice {\\n  id\\n  questionID\\n  detail\\n  mediaID\\n  position\\n  stats\\n  __typename\\n}\\n\\nfragment Gallery on Gallery {\\n  name\\n  description\\n  __typename\\n  id\\n  createdAt\\n  updatedAt\\n  galleryMedias {\\n    ...GalleryMedia\\n    __typename\\n  }\\n}\\n\\nfragment GalleryMedia on GalleryMedia {\\n  mediaID\\n  description\\n  caption\\n  media {\\n    ...Media\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment Media on Media {\\n  id\\n  title\\n  description\\n  publicID\\n  externalURL\\n  awsS3Key\\n  height\\n  width\\n  locationName\\n  locationLat\\n  locationLon\\n  mediaType\\n  mediaSourceID\\n  photographer\\n  eventDate\\n  internalTags\\n  __typename\\n}\\n\\nfragment Form on Form {\\n  __typename\\n  id\\n  title\\n  description\\n  generateStatus\\n  lastGenerated\\n  createdBy {\\n    ...SimpleUser\\n    __typename\\n  }\\n  createdAt\\n  updatedAt\\n  endDate\\n  pages {\\n    ...FormPage\\n    __typename\\n  }\\n  respondent\\n  coverMedia {\\n    ...Media\\n    __typename\\n  }\\n  accentColor\\n  backgroundColor\\n  completedResponseTitle\\n  completedResponse\\n  formResponseConfirmationMail {\\n    ...FormResponseConfirmationMail\\n    __typename\\n  }\\n}\\n\\nfragment FormPage on FormPage {\\n  page\\n  questions {\\n    ...FormQuestion\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment FormQuestion on FormQuestion {\\n  id\\n  formId\\n  type\\n  description\\n  title\\n  isRequired\\n  createdAt\\n  updatedAt\\n  addOns {\\n    ...QuestionAddOns\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment QuestionAddOns on QuestionAddOns {\\n  colcount\\n  choicesOrder\\n  hasOther\\n  otherText\\n  optionsCaption\\n  hasNone\\n  hasSelectAll\\n  valueTrue\\n  valueFalse\\n  labelTrue\\n  labelFalse\\n  rows\\n  placeholder\\n  choices\\n  fileType\\n  __typename\\n}\\n\\nfragment SimpleUser on User {\\n  __typename\\n  id\\n  name\\n  username\\n  aboutMe\\n  isVerified\\n  profilePictureMedia {\\n    ...Media\\n    __typename\\n  }\\n  coverPictureMedia {\\n    ...Media\\n    __typename\\n  }\\n  status\\n  metaKeyword\\n  metaTitle\\n  metaDescription\\n  excludedFromSitemapAt\\n}\\n\\nfragment FormResponseConfirmationMail on FormResponseConfirmationMail {\\n  title\\n  header\\n  body\\n  hyperlinkURL\\n  hyperlinkTitle\\n  __typename\\n}\\n\\nfragment Recipe on Recipe {\\n  id\\n  name\\n  description\\n  mediaID\\n  media {\\n    ...Media\\n    __typename\\n  }\\n  portionSize\\n  ingredients\\n  instructions {\\n    step\\n    instruction\\n    __typename\\n  }\\n  cookingTime\\n  calories\\n  createdAt\\n  createdBy {\\n    ...SimpleUser\\n    __typename\\n  }\\n  updatedAt\\n  updatedBy {\\n    ...SimpleUser\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment LiveBlogAddOnDetail on LiveBlog {\\n  id\\n  startsAt\\n  endsAt\\n  updatedAt\\n  __typename\\n}\\n\\nfragment Headline on Headline {\\n  storyID\\n  desktopMedia {\\n    ...Media\\n    __typename\\n  }\\n  mobileMedia {\\n    ...Media\\n    __typename\\n  }\\n  startTime\\n  endTime\\n  __typename\\n}\\n\\nfragment StoryStatistic on StoryStatistic {\\n  storyID\\n  commentCount\\n  likeCount\\n  __typename\\n}\\n\\nfragment SimplePublisher on Publisher {\\n  __typename\\n  id\\n  name\\n  slug\\n  description\\n  isVerified\\n  isPremium\\n  avatarMedia {\\n    ...Media\\n    __typename\\n  }\\n  instagramURL\\n  twitterURL\\n  facebookURL\\n  website\\n  isCorporateSubscriber\\n  organisation {\\n    ...Organisation\\n    __typename\\n  }\\n}\\n\\nfragment Organisation on Organisation {\\n  id\\n  name\\n  phone1\\n  address\\n  email\\n  companyName\\n  editorialInChief\\n  editorialCompositions {\\n    ...EditorialComposition\\n    __typename\\n  }\\n  organisationType\\n  __typename\\n}\\n\\nfragment EditorialComposition on EditorialComposition {\\n  id\\n  organisationID\\n  position\\n  names\\n  order\\n  __typename\\n}\\n\\nfragment Product on Product {\\n  id\\n  sku\\n  objectID\\n  objectType\\n  object {\\n    ... on Story {\\n      __typename\\n      id\\n      slug\\n      title\\n      leadText\\n      author {\\n        id\\n        username\\n        __typename\\n      }\\n      publisher {\\n        id\\n        slug\\n        __typename\\n      }\\n      leadMedia {\\n        id\\n        externalURL\\n        __typename\\n      }\\n    }\\n    ... on Collection {\\n      __typename\\n      id\\n      slug\\n      title\\n      description\\n      coverMedia {\\n        id\\n        externalURL\\n        __typename\\n      }\\n    }\\n    ... on SubscriptionPackage {\\n      ...SimpleSubscriptionPackage\\n      __typename\\n    }\\n    __typename\\n  }\\n  normalPrice {\\n    ...Money\\n    __typename\\n  }\\n  price {\\n    ...Money\\n    __typename\\n  }\\n  taxCategory {\\n    ...TaxCategory\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment TaxCategory on TaxCategory {\\n  id\\n  name\\n  rateInPercentage\\n  isInclusive\\n  __typename\\n}\\n\\nfragment Money on Money {\\n  currencyCode\\n  units\\n  cents\\n  __typename\\n}\\n\\nfragment SimpleSubscriptionPackage on SubscriptionPackage {\\n  __typename\\n  id\\n  name\\n  subscriptionDescription: description\\n  isActive\\n  isRecurring\\n  period\\n  periodType\\n  gracePeriodInSeconds\\n  platform\\n}\\n\\nfragment CompactCollection on Collection {\\n  __typename\\n  id\\n  title\\n  isPinned\\n  slug\\n  createdAt\\n  readEligibility\\n  updatedAt\\n  lastStoryAddedAt\\n  description\\n  category\\n  coverMedia {\\n    ...Media\\n    __typename\\n  }\\n  readEligibility\\n  productInfo {\\n    ...Product\\n    __typename\\n  }\\n  storiesCount\\n  videoDescriptionLink\\n  premiumInfo {\\n    setPremiumAt\\n    __typename\\n  }\\n  kumparanPlusInfo {\\n    createdAt\\n    __typename\\n  }\\n  highlightedMenus {\\n    url\\n    title\\n    __typename\\n  }\\n  topics {\\n    ...Topic\\n    __typename\\n  }\\n  firstStoryAddedAt\\n  lastStoryAddedAt\\n}\\n\\nfragment Topic on Topic {\\n  __typename\\n  id\\n  name\\n  slug\\n  description\\n  isPremium\\n  coverMedia {\\n    ...Media\\n    __typename\\n  }\\n  metaName\\n  metaKeywordsV2\\n  metaDescription\\n  createdAt\\n  updatedAt\\n}\\n\\nfragment CursorInfo on CursorInfo {\\n  size\\n  count\\n  countPage\\n  hasMore\\n  cursor\\n  cursorType\\n  nextCursor\\n  __typename\\n}\\n\"}]";
                
                // String jsonBody = "[{\"operationName\":\"FindStoryFeedByChannelSlug\",\"variables\":{\"channelSlug\":\"news\",\"cursor\":\"1\",\"size\":25,\"cursorType\":\"PAGE\",\"userAliasID\":\"53273844-33bb-4fa0-b20a-eacd9a0892d6\"},\"query\":\"query FindStoryFeedByChannelSlug($channelSlug: String!, $userAliasID: ID, $size: Int!, $cursor: String!, $cursorType: CursorType!) {\\n  FindStoryFeedByChannelSlug(\\n    channelSlug: $channelSlug\\n    userAliasID: $userAliasID\\n    cursorType: $cursorType\\n    size: $size\\n    cursor: $cursor\\n  ) {\\n    edges {\\n      ...CompactStory\\n      __typename\\n    }\\n    cursorInfo {\\n      ...CursorInfo\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\\nfragment CompactStory on Story {\\n  __typename\\n  id\\n  authorID\\n  title\\n  customTitle\\n  createdAt\\n  leadText\\n  publishedAt\\n  isAgeRestrictedContent\\n  slug\\n  isStickyStory\\n  isShowOnWeb\\n  isShowOnApp\\n  isDisableComment\\n  isDisableLike\\n  isDisableShare\\n  isSnackable\\n  metaDescription\\n  readTimeInMinutes\\n  storyAddOns {\\n    ...StoryAddOn\\n    __typename\\n  }\\n  author {\\n    ...SimpleUser\\n    __typename\\n  }\\n  publisher {\\n    ...SimplePublisher\\n    __typename\\n  }\\n  leadMedia {\\n    ...Media\\n    __typename\\n  }\\n  headline {\\n    ...Headline\\n    __typename\\n  }\\n  statistic {\\n    ...StoryStatistic\\n    __typename\\n  }\\n  readEligibility\\n  productInfo {\\n    ...Product\\n    __typename\\n  }\\n  isPrivate\\n  collection {\\n    ...CompactCollection\\n    __typename\\n  }\\n  internalTags\\n  deletedAt\\n  deletionInfo {\\n    __typename\\n    deleterType\\n    deletedBy {\\n      __typename\\n      id\\n      name\\n    }\\n  }\\n}\\n\\nfragment StoryAddOn on StoryAddOn {\\n  object {\\n    __typename\\n    ... on Polling {\\n      ...Polling\\n      __typename\\n    }\\n    ... on Gallery {\\n      ...Gallery\\n      __typename\\n    }\\n    ... on Form {\\n      ...Form\\n      __typename\\n    }\\n    ... on Recipe {\\n      ...Recipe\\n      __typename\\n    }\\n    ... on LiveBlog {\\n      ...LiveBlogAddOnDetail\\n      __typename\\n    }\\n  }\\n  addOnType\\n  __typename\\n}\\n\\nfragment Polling on Polling {\\n  __typename\\n  id\\n  name\\n  description\\n  mediaID\\n  startsAt\\n  endsAt\\n  questions {\\n    ...Question\\n    __typename\\n  }\\n}\\n\\nfragment Question on Question {\\n  id\\n  pollingID\\n  detail\\n  position\\n  choices {\\n    ...Choice\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment Choice on Choice {\\n  id\\n  questionID\\n  detail\\n  mediaID\\n  position\\n  stats\\n  __typename\\n}\\n\\nfragment Gallery on Gallery {\\n  name\\n  description\\n  __typename\\n  id\\n  createdAt\\n  updatedAt\\n  galleryMedias {\\n    ...GalleryMedia\\n    __typename\\n  }\\n}\\n\\nfragment GalleryMedia on GalleryMedia {\\n  mediaID\\n  description\\n  caption\\n  media {\\n    ...Media\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment Media on Media {\\n  id\\n  title\\n  description\\n  publicID\\n  externalURL\\n  awsS3Key\\n  height\\n  width\\n  locationName\\n  locationLat\\n  locationLon\\n  mediaType\\n  mediaSourceID\\n  photographer\\n  eventDate\\n  internalTags\\n  __typename\\n}\\n\\nfragment Form on Form {\\n  __typename\\n  id\\n  title\\n  description\\n  generateStatus\\n  lastGenerated\\n  createdBy {\\n    ...SimpleUser\\n    __typename\\n  }\\n  createdAt\\n  updatedAt\\n  endDate\\n  pages {\\n    ...FormPage\\n    __typename\\n  }\\n  respondent\\n  coverMedia {\\n    ...Media\\n    __typename\\n  }\\n  accentColor\\n  backgroundColor\\n  completedResponseTitle\\n  completedResponse\\n  formResponseConfirmationMail {\\n    ...FormResponseConfirmationMail\\n    __typename\\n  }\\n}\\n\\nfragment FormPage on FormPage {\\n  page\\n  questions {\\n    ...FormQuestion\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment FormQuestion on FormQuestion {\\n  id\\n  formId\\n  type\\n  description\\n  title\\n  isRequired\\n  createdAt\\n  updatedAt\\n  addOns {\\n    ...QuestionAddOns\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment QuestionAddOns on QuestionAddOns {\\n  colcount\\n  choicesOrder\\n  hasOther\\n  otherText\\n  optionsCaption\\n  hasNone\\n  hasSelectAll\\n  valueTrue\\n  valueFalse\\n  labelTrue\\n  labelFalse\\n  rows\\n  placeholder\\n  choices\\n  fileType\\n  __typename\\n}\\n\\nfragment SimpleUser on User {\\n  __typename\\n  id\\n  name\\n  username\\n  aboutMe\\n  isVerified\\n  profilePictureMedia {\\n    ...Media\\n    __typename\\n  }\\n  coverPictureMedia {\\n    ...Media\\n    __typename\\n  }\\n  status\\n  metaKeyword\\n  metaTitle\\n  metaDescription\\n  excludedFromSitemapAt\\n}\\n\\nfragment FormResponseConfirmationMail on FormResponseConfirmationMail {\\n  title\\n  header\\n  body\\n  hyperlinkURL\\n  hyperlinkTitle\\n  __typename\\n}\\n\\nfragment Recipe on Recipe {\\n  id\\n  name\\n  description\\n  mediaID\\n  media {\\n    ...Media\\n    __typename\\n  }\\n  portionSize\\n  ingredients\\n  instructions {\\n    step\\n    instruction\\n    __typename\\n  }\\n  cookingTime\\n  calories\\n  createdAt\\n  createdBy {\\n    ...SimpleUser\\n    __typename\\n  }\\n  updatedAt\\n  updatedBy {\\n    ...SimpleUser\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment LiveBlogAddOnDetail on LiveBlog {\\n  id\\n  startsAt\\n  endsAt\\n  updatedAt\\n  __typename\\n}\\n\\nfragment Headline on Headline {\\n  storyID\\n  desktopMedia {\\n    ...Media\\n    __typename\\n  }\\n  mobileMedia {\\n    ...Media\\n    __typename\\n  }\\n  startTime\\n  endTime\\n  __typename\\n}\\n\\nfragment StoryStatistic on StoryStatistic {\\n  storyID\\n  commentCount\\n  likeCount\\n  __typename\\n}\\n\\nfragment SimplePublisher on Publisher {\\n  __typename\\n  id\\n  name\\n  slug\\n  description\\n  isVerified\\n  isPremium\\n  avatarMedia {\\n    ...Media\\n    __typename\\n  }\\n  instagramURL\\n  twitterURL\\n  facebookURL\\n  website\\n  isCorporateSubscriber\\n  organisation {\\n    ...Organisation\\n    __typename\\n  }\\n}\\n\\nfragment Organisation on Organisation {\\n  id\\n  name\\n  phone1\\n  address\\n  email\\n  companyName\\n  editorialInChief\\n  editorialCompositions {\\n    ...EditorialComposition\\n    __typename\\n  }\\n  organisationType\\n  __typename\\n}\\n\\nfragment EditorialComposition on EditorialComposition {\\n  id\\n  organisationID\\n  position\\n  names\\n  order\\n  __typename\\n}\\n\\nfragment Product on Product {\\n  id\\n  sku\\n  objectID\\n  objectType\\n  object {\\n    ... on Story {\\n      __typename\\n      id\\n      slug\\n      title\\n      leadText\\n      author {\\n        id\\n        username\\n        __typename\\n      }\\n      publisher {\\n        id\\n        slug\\n        __typename\\n      }\\n      leadMedia {\\n        id\\n        externalURL\\n        __typename\\n      }\\n    }\\n    ... on Collection {\\n      __typename\\n      id\\n      slug\\n      title\\n      description\\n      coverMedia {\\n        id\\n        externalURL\\n        __typename\\n      }\\n    }\\n    ... on SubscriptionPackage {\\n      ...SimpleSubscriptionPackage\\n      __typename\\n    }\\n    __typename\\n  }\\n  normalPrice {\\n    ...Money\\n    __typename\\n  }\\n  price {\\n    ...Money\\n    __typename\\n  }\\n  taxCategory {\\n    ...TaxCategory\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment TaxCategory on TaxCategory {\\n  id\\n  name\\n  rateInPercentage\\n  isInclusive\\n  __typename\\n}\\n\\nfragment Money on Money {\\n  currencyCode\\n  units\\n  cents\\n  __typename\\n}\\n\\nfragment SimpleSubscriptionPackage on SubscriptionPackage {\\n  __typename\\n  id\\n  name\\n  subscriptionDescription: description\\n  isActive\\n  isRecurring\\n  period\\n  periodType\\n  gracePeriodInSeconds\\n  platform\\n}\\n\\nfragment CompactCollection on Collection {\\n  __typename\\n  id\\n  title\\n  isPinned\\n  slug\\n  createdAt\\n  readEligibility\\n  updatedAt\\n  lastStoryAddedAt\\n  description\\n  category\\n  coverMedia {\\n    ...Media\\n    __typename\\n  }\\n  readEligibility\\n  productInfo {\\n    ...Product\\n    __typename\\n  }\\n  storiesCount\\n  videoDescriptionLink\\n  premiumInfo {\\n    setPremiumAt\\n    __typename\\n  }\\n  kumparanPlusInfo {\\n    createdAt\\n    __typename\\n  }\\n  highlightedMenus {\\n    url\\n    title\\n    __typename\\n  }\\n  topics {\\n    ...Topic\\n    __typename\\n  }\\n  firstStoryAddedAt\\n  lastStoryAddedAt\\n}\\n\\nfragment Topic on Topic {\\n  __typename\\n  id\\n  name\\n  slug\\n  description\\n  isPremium\\n  coverMedia {\\n    ...Media\\n    __typename\\n  }\\n  metaName\\n  metaKeywordsV2\\n  metaDescription\\n  createdAt\\n  updatedAt\\n}\\n\\nfragment CursorInfo on CursorInfo {\\n  size\\n  count\\n  countPage\\n  hasMore\\n  cursor\\n  cursorType\\n  nextCursor\\n  __typename\\n}\\n\"}]";
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonBody.getBytes());
        
                Request request = new Request.Builder()
                        .url(url)
                        .header("authority", "graphql-v4.kumparan.com")
                        .header("accept", "*/*")
                        .header("accept-language", "en-US,en;q=0.9")
                        .header("content-type", "text/plain")
                        .header("deduplicate", "1")
                        .header("env-client", "a1833e44e2c236f8b39903ef49b856d5ebf05efdd8ef4513e58db32dfdeabe7299d15d1e7976b314efd400aca5fafeb1")
                        .header("origin", "https://kumparan.com")
                        .header("referer", "https://kumparan.com/")
                        .header("sec-ch-ua", "\"Not A(Brand\";v=\"99\", \"Google Chrome\";v=\"121\", \"Chromium\";v=\"121\"")
                        .header("sec-ch-ua-mobile", "?0")
                        .header("sec-ch-ua-platform", "\"Linux\"")
                        .header("sec-fetch-dest", "empty")
                        .header("sec-fetch-mode", "cors")
                        .header("sec-fetch-site", "same-site")
                        .header("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36")
                        .post(requestBody)
                        .build();

                try (Response response = client.newCall(request).execute()) {
                    JSONArray json = new JSONArray(response.body().string()).getJSONObject(0).getJSONObject("data").getJSONObject("FindStoryFeedByChannelSlug").getJSONArray("edges");
                    for (int i = 0; i < json.length(); i++) {
                        String link = json.getJSONObject(i).getString("slug");
                        linkList.add("https://kumparan.com/kumparannews/"+link);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
            }

            for(String href : linkList) {
                JSONObject jsonUrl = createJsonKafkaUrl(href, baseUrl, domain, domain, depth, "", "");
                if(href.length()!=0 && depth <=4 ) {
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }      
            }

            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    kumparanScrapper(newLandingUrl, newUrl, newDepth);
                }
            }              

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Kumparan.com");
        }
    }


    public void kumparanScrapper(String landingUrl, String url, int depth) throws Exception {
        try {
            String domain = "kumparan.com";
            depth += 1;
            String topic = "news-topic-kumparan";
            System.out.println("Processing kumparan.com: Parsing Content and Related News");
            String graphqlUrl = "https://cdn-graphql-v4.kumparan.com/query";

            // Parsing ID from request body of HttpRequest to parsing related news
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                .url(graphqlUrl+"?operationName=FindStoryBySlug&variables=%7B%22slug%22%3A%22"+url.replace("https://kumparan.com/kumparannews/", "")+"%22%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22b3682edacea1005cd9ca552238f9e7e36d683fe476fe6ffe4ea52c038f613865%22%7D%7D&cache-ttl=10")
                .header("authority", "cdn-graphql-v4.kumparan.com")
                .header("accept", "*/*")
                .header("accept-language", "en-US,en;q=0.9")
                .header("content-type", "text/plain")
                .header("deduplicate", "1")
                .header("env-client", "a1833e44e2c236f8b39903ef49b856d57f7b18581a61dde7df2cb72eea59bf0d4d04df58f6e384797fdd5a2447aae47c")
                .header("if-modified-since", "Mon, 29 Jan 2024 02:34:54 GMT")
                .header("origin", "https://kumparan.com")
                .header("referer", "https://kumparan.com/")
                .header("sec-ch-ua", "\"Not A(Brand\";v=\"99\", \"Google Chrome\";v=\"121\", \"Chromium\";v=\"121\"")
                .header("sec-ch-ua-mobile", "?0")
                .header("sec-ch-ua-platform", "\"Linux\"")
                .header("sec-fetch-dest", "empty")
                .header("sec-fetch-mode", "cors")
                .header("sec-fetch-site", "same-site")
                .header("user-agent", userAgent)
                .build();

            try {
                
                Response response = client.newCall(request).execute();
                String newsId = new JSONObject(response.body().string()).getJSONObject("data").getJSONObject("FindStoryBySlug").getString("id");

                OkHttpClient clientUrl = new OkHttpClient();

                String graphqlQuery = "[{\"operationName\":\"FindRelatedStories\",\"variables\":{\"size\":3,\"storyID\":\"" + newsId + "\",\"userAliasID\":\"53273844-33bb-4fa0-b20a-eacd9a0892d6\"},\"query\":\"query FindRelatedStories($storyID: ID!, $size: Int!, $userAliasID: ID) {\\n  FindRelatedStories(\\n    storyID: $storyID\\n    size: $size\\n    cursorType: PAGE\\n    cursor: \\\"1\\\"\\n    userAliasID: $userAliasID\\n  ) {\\n    edges {\\n      ...CompactStory\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\\nfragment CompactStory on Story {\\n  __typename\\n  id\\n  authorID\\n  title\\n  customTitle\\n  createdAt\\n  leadText\\n  publishedAt\\n  isAgeRestrictedContent\\n  slug\\n  isStickyStory\\n  isShowOnWeb\\n  isShowOnApp\\n  isDisableComment\\n  isDisableLike\\n  isDisableShare\\n  isSnackable\\n  metaDescription\\n  readTimeInMinutes\\n  storyAddOns {\\n    ...StoryAddOn\\n    __typename\\n  }\\n  author {\\n    ...SimpleUser\\n    __typename\\n  }\\n  publisher {\\n    ...SimplePublisher\\n    __typename\\n  }\\n  leadMedia {\\n    ...Media\\n    __typename\\n  }\\n  headline {\\n    ...Headline\\n    __typename\\n  }\\n  statistic {\\n    ...StoryStatistic\\n    __typename\\n  }\\n  readEligibility\\n  productInfo {\\n    ...Product\\n    __typename\\n  }\\n  isPrivate\\n  collection {\\n    ...CompactCollection\\n    __typename\\n  }\\n  internalTags\\n  deletedAt\\n  deletionInfo {\\n    __typename\\n    deleterType\\n    deletedBy {\\n      __typename\\n      id\\n      name\\n    }\\n  }\\n}\\n\\nfragment StoryAddOn on StoryAddOn {\\n  object {\\n    __typename\\n    ... on Polling {\\n      ...Polling\\n      __typename\\n    }\\n    ... on Gallery {\\n      ...Gallery\\n      __typename\\n    }\\n    ... on Form {\\n      ...Form\\n      __typename\\n    }\\n    ... on Recipe {\\n      ...Recipe\\n      __typename\\n    }\\n    ... on LiveBlog {\\n      ...LiveBlogAddOnDetail\\n      __typename\\n    }\\n  }\\n  addOnType\\n  __typename\\n}\\n\\nfragment Polling on Polling {\\n  __typename\\n  id\\n  name\\n  description\\n  mediaID\\n  startsAt\\n  endsAt\\n  questions {\\n    ...Question\\n    __typename\\n  }\\n}\\n\\nfragment Question on Question {\\n  id\\n  pollingID\\n  detail\\n  position\\n  choices {\\n    ...Choice\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment Choice on Choice {\\n  id\\n  questionID\\n  detail\\n  mediaID\\n  position\\n  stats\\n  __typename\\n}\\n\\nfragment Gallery on Gallery {\\n  name\\n  description\\n  __typename\\n  id\\n  createdAt\\n  updatedAt\\n  galleryMedias {\\n    ...GalleryMedia\\n    __typename\\n  }\\n}\\n\\nfragment GalleryMedia on GalleryMedia {\\n  mediaID\\n  description\\n  caption\\n  media {\\n    ...Media\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment Media on Media {\\n  id\\n  title\\n  description\\n  publicID\\n  externalURL\\n  awsS3Key\\n  height\\n  width\\n  locationName\\n  locationLat\\n  locationLon\\n  mediaType\\n  mediaSourceID\\n  photographer\\n  eventDate\\n  internalTags\\n  __typename\\n}\\n\\nfragment Form on Form {\\n  __typename\\n  id\\n  title\\n  description\\n  generateStatus\\n  lastGenerated\\n  createdBy {\\n    ...SimpleUser\\n    __typename\\n  }\\n  createdAt\\n  updatedAt\\n  endDate\\n  pages {\\n    ...FormPage\\n    __typename\\n  }\\n  respondent\\n  coverMedia {\\n    ...Media\\n    __typename\\n  }\\n  accentColor\\n  backgroundColor\\n  completedResponseTitle\\n  completedResponse\\n  formResponseConfirmationMail {\\n    ...FormResponseConfirmationMail\\n    __typename\\n  }\\n}\\n\\nfragment FormPage on FormPage {\\n  page\\n  questions {\\n    ...FormQuestion\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment FormQuestion on FormQuestion {\\n  id\\n  formId\\n  type\\n  description\\n  title\\n  isRequired\\n  createdAt\\n  updatedAt\\n  addOns {\\n    ...QuestionAddOns\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment QuestionAddOns on QuestionAddOns {\\n  colcount\\n  choicesOrder\\n  hasOther\\n  otherText\\n  optionsCaption\\n  hasNone\\n  hasSelectAll\\n  valueTrue\\n  valueFalse\\n  labelTrue\\n  labelFalse\\n  rows\\n  placeholder\\n  choices\\n  fileType\\n  __typename\\n}\\n\\nfragment SimpleUser on User {\\n  __typename\\n  id\\n  name\\n  username\\n  aboutMe\\n  isVerified\\n  profilePictureMedia {\\n    ...Media\\n    __typename\\n  }\\n  coverPictureMedia {\\n    ...Media\\n    __typename\\n  }\\n  status\\n  metaKeyword\\n  metaTitle\\n  metaDescription\\n  excludedFromSitemapAt\\n}\\n\\nfragment FormResponseConfirmationMail on FormResponseConfirmationMail {\\n  title\\n  header\\n  body\\n  hyperlinkURL\\n  hyperlinkTitle\\n  __typename\\n}\\n\\nfragment Recipe on Recipe {\\n  id\\n  name\\n  description\\n  mediaID\\n  media {\\n    ...Media\\n    __typename\\n  }\\n  portionSize\\n  ingredients\\n  instructions {\\n    step\\n    instruction\\n    __typename\\n  }\\n  cookingTime\\n  calories\\n  createdAt\\n  createdBy {\\n    ...SimpleUser\\n    __typename\\n  }\\n  updatedAt\\n  updatedBy {\\n    ...SimpleUser\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment LiveBlogAddOnDetail on LiveBlog {\\n  id\\n  startsAt\\n  endsAt\\n  updatedAt\\n  __typename\\n}\\n\\nfragment Headline on Headline {\\n  storyID\\n  desktopMedia {\\n    ...Media\\n    __typename\\n  }\\n  mobileMedia {\\n    ...Media\\n    __typename\\n  }\\n  startTime\\n  endTime\\n  __typename\\n}\\n\\nfragment StoryStatistic on StoryStatistic {\\n  storyID\\n  commentCount\\n  likeCount\\n  __typename\\n}\\n\\nfragment SimplePublisher on Publisher {\\n  __typename\\n  id\\n  name\\n  slug\\n  description\\n  isVerified\\n  isPremium\\n  avatarMedia {\\n    ...Media\\n    __typename\\n  }\\n  instagramURL\\n  twitterURL\\n  facebookURL\\n  website\\n  isCorporateSubscriber\\n  organisation {\\n    ...Organisation\\n    __typename\\n  }\\n}\\n\\nfragment Organisation on Organisation {\\n  id\\n  name\\n  phone1\\n  address\\n  email\\n  companyName\\n  editorialInChief\\n  editorialCompositions {\\n    ...EditorialComposition\\n    __typename\\n  }\\n  organisationType\\n  __typename\\n}\\n\\nfragment EditorialComposition on EditorialComposition {\\n  id\\n  organisationID\\n  position\\n  names\\n  order\\n  __typename\\n}\\n\\nfragment Product on Product {\\n  id\\n  sku\\n  objectID\\n  objectType\\n  object {\\n    ... on Story {\\n      __typename\\n      id\\n      slug\\n      title\\n      leadText\\n      author {\\n        id\\n        username\\n        __typename\\n      }\\n      publisher {\\n        id\\n        slug\\n        __typename\\n      }\\n      leadMedia {\\n        id\\n        externalURL\\n        __typename\\n      }\\n    }\\n    ... on Collection {\\n      __typename\\n      id\\n      slug\\n      title\\n      description\\n      coverMedia {\\n        id\\n        externalURL\\n        __typename\\n      }\\n    }\\n    ... on SubscriptionPackage {\\n      ...SimpleSubscriptionPackage\\n      __typename\\n    }\\n    __typename\\n  }\\n  normalPrice {\\n    ...Money\\n    __typename\\n  }\\n  price {\\n    ...Money\\n    __typename\\n  }\\n  taxCategory {\\n    ...TaxCategory\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment TaxCategory on TaxCategory {\\n  id\\n  name\\n  rateInPercentage\\n  isInclusive\\n  __typename\\n}\\n\\nfragment Money on Money {\\n  currencyCode\\n  units\\n  cents\\n  __typename\\n}\\n\\nfragment SimpleSubscriptionPackage on SubscriptionPackage {\\n  __typename\\n  id\\n  name\\n  subscriptionDescription: description\\n  isActive\\n  isRecurring\\n  period\\n  periodType\\n  gracePeriodInSeconds\\n  platform\\n}\\n\\nfragment CompactCollection on Collection {\\n  __typename\\n  id\\n  title\\n  isPinned\\n  slug\\n  createdAt\\n  readEligibility\\n  updatedAt\\n  lastStoryAddedAt\\n  description\\n  category\\n  coverMedia {\\n    ...Media\\n    __typename\\n  }\\n  readEligibility\\n  productInfo {\\n    ...Product\\n    __typename\\n  }\\n  storiesCount\\n  videoDescriptionLink\\n  premiumInfo {\\n    setPremiumAt\\n    __typename\\n  }\\n  kumparanPlusInfo {\\n    createdAt\\n    __typename\\n  }\\n  highlightedMenus {\\n    url\\n    title\\n    __typename\\n  }\\n  topics {\\n    ...Topic\\n    __typename\\n  }\\n  firstStoryAddedAt\\n  lastStoryAddedAt\\n}\\n\\nfragment Topic on Topic {\\n  __typename\\n  id\\n  name\\n  slug\\n  description\\n  isPremium\\n  coverMedia {\\n    ...Media\\n    __typename\\n  }\\n  metaName\\n  metaKeywordsV2\\n  metaDescription\\n  createdAt\\n  updatedAt\\n}\\n\"}]";

                RequestBody requestBody = RequestBody.create(MediaType.get("text/plain"), graphqlQuery);
        
                Request requestBacaJuga = new Request.Builder()
                    .url(graphqlUrl)
                    .addHeader("authority", "graphql-v4.kumparan.com")
                    .addHeader("accept", "*/*")
                    .addHeader("accept-language", "en-US,en;q=0.9")
                    .addHeader("content-type", "text/plain")
                    .addHeader("env-client", "a1833e44e2c236f8b39903ef49b856d5af512d0140c4515d2c9faaa17203e7454b9729835f1f38b75bd676d12070c815")
                    .addHeader("origin", "https://kumparan.com")
                    .addHeader("referer", "https://kumparan.com/")
                    .addHeader("sec-ch-ua", "\"Not A(Brand\";v=\"99\", \"Google Chrome\";v=\"121\", \"Chromium\";v=\"121\"")
                    .addHeader("sec-ch-ua-mobile", "?0")
                    .addHeader("sec-ch-ua-platform", "\"Linux\"")
                    .addHeader("sec-fetch-dest", "empty")
                    .addHeader("sec-fetch-mode", "cors")
                    .addHeader("sec-fetch-site", "same-site")
                    .addHeader("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36")
                    .post(requestBody)
                    .build();
        
                try {
                    Response responseBacaJuga = clientUrl.newCall(requestBacaJuga).execute();
                    JSONArray jsonArray = new JSONArray(responseBacaJuga.body().string()).getJSONObject(0).getJSONObject("data").getJSONObject("FindRelatedStories").getJSONArray("edges");
                    for (int k = 0; k < jsonArray.length(); k++) {
                        String hrefRelatedNews = jsonArray.getJSONObject(k).getString("slug");
                        JSONObject jsonUrl = createJsonKafkaUrl("https://kumparan.com/kumparannews/"+hrefRelatedNews, url, domain, domain, depth, "", "");
                        if(hrefRelatedNews.length()!=0 && depth <=4 ) {
                            kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                        }
                    }

                    // Get list of JSON Url from Kafka
                    List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

                    // Iterate list of url to get content
                    for (int i = 0; i < jsonFromKafka.size(); i++) {
                        // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                        JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                        JsonObject json = jsonElement.getAsJsonObject();

                        String newUrl = json.get("url").getAsString();
                        int newDepth = json.get("depth").getAsInt();
                        String newLandingUrl = json.get("landing_url").getAsString();

                        if(newDepth <= 4) {
                            kumparanScrapper(newLandingUrl, newUrl, newDepth);
                        }
                    }    
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            // Parsing using Jsoup
            Document document = Jsoup.connect(url).userAgent(userAgent).get();
            Elements elements = document.select("div.StoryRenderer__EditorWrapper-sc-1f9fbz3-0.ghydBU div div div span");
            Elements paragraphs = elements.select("span:not(:contains(Perbesar)):not(:contains(ADVERTISEMENT))");

            String content = "";
            for(Element paragraph : paragraphs) {
                if(paragraph.text().length()!=0)
                    content += paragraph.text();
            }

            JSONObject jsonContent = createJsonKafkaContent(url, domain, content);

            if(content.length()!=0 && depth <=4) 
                kafkaService.sendContentToKafka(jsonContent.toString());

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping Kumparan.com");
        }
    }

    public void tvonenewsIndex() throws Exception {
        try {
            String domain = "tvonenews.com";
            CrawlMedia crawlMedia = crawlMediaRepository.getByDomain(domain);
            String baseUrl = crawlMedia.getLandingUrl();
            int depth = 0;
            String topic = "news-topic-tvonenews";
            System.out.println("Processing tvonenews.com: Parsing Index Page");
            String dateUrl1 = dateFormatter("yyyy-MM-dd");
            String dateUrl2 = dateFormatter("yyyy/MM/dd");

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8");
            for (int i = 1; i <= 3; i++) {

                RequestBody body = RequestBody.create(mediaType, "last_publish_date="+dateUrl1+"07%3A00%3A23&channel_name=berita&subchannel_name=all&page="+i+"&type=art&record_count=12&_token=77618PRwzV9cJ5KCdhlWXiEYgjo9U9UqWv1qfjLM");
    
                Request request = new Request.Builder()
                        .url("https://www.tvonenews.com/request/load_indeks_article")
                        .method("POST", body)
                        .addHeader("authority", "www.tvonenews.com")
                        .addHeader("accept", "*/*")
                        .addHeader("accept-language", "en-US,en;q=0.9")
                        .addHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
                        .addHeader("cookie", "_cc_id=1c53434c430056ffe2701409b0d15c7f; cto_bundle=eljo7V9RbEdjNDdFMzVwUkR5aE1oWExjaGJFWE55aXI5UzZpREVHY1pjSzBqYiUyRjdRRVlwRUxMY0kyVmZmUVVLT0diT1d4NlBlU1BiNEE3UHlQTTY3UXJqNGtPOWxmVFQ1ZFVjeHZ1eGYlMkZod0JudTVJbjA2Qnk3NmZ0YUpCaElpWUV1QU50OFR2ZERqUkNoZUlETG1XTm0wJTJCemdXZiUyQjBRJTJCbmxyRGZHaWpybHVRTEN1SUxDc0tMMkNRNUxoUW5iemJFSm9Fa2VnUG5aNnRwMzVuVWhSRXAyRDM0amolMkZoZmsyTHVzUjl4UVBQcFhHZERwYUV2WlA5OEwyOFFvNmVySjJLT24xZmNhMVNub0hSWlc4eW9mUHNmMmpaWUh1eUxZZkh6TUwlMkZzRnlVV2tGdk9odG93amR4T1ZtNURTZ044Y3pEcGlO; _ss_pp_id=d15475d9121217e21d71695016827599; __utmz=262966473.1706517353.7.6.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); _gid=GA1.2.109274476.1706690624; panoramaId_expiry=1707295428233; panoramaId=099fc4901cb08971b3f07f8a5da3185ca02c233abff817e02d3c254cacbef90f; panoramaIdType=panoDevice; _td=2b93d62c-a4fb-486e-8b88-0f873db49129; _gat=1; __utma=262966473.1083217.1706077335.1706690625.1706750313.9; __utmc=262966473; __utmt_UA-158515037-1=1; _clck=12im9dg%7C2%7Cfiw%7C0%7C1484; 651dfdf3-f6cf-4dce-b88d-f9b4e8d85861=2c614bb1ef5d20dedfe09fdc059c6425; _ga_1F8KC7SHMP=GS1.1.1706750312.9.1.1706750341.31.0.0; _ga=GA1.1.1083217.1706077335; __utmb=262966473.5.10.1706750313; _ga_SKS0GZ01Z9=GS1.2.1706750312.9.1.1706750341.0.0.0; XSRF-TOKEN=eyJpdiI6IkIxUXJDWkZCS0h2SGhDbDA2VTNcL3d3PT0iLCJ2YWx1ZSI6IkJtbm90ZDF1RzkzTnFjYTdNWmVOemxYTXJ3N0pQMFdNRzlISUNVXC9YK1hnUGMwVjRZMHRIWjhIRVVtdkZOWjFTIiwibWFjIjoiZGIxMWQ4ZmM1ZDBlYWU5ODVmYTRjM2ExOWNhMTc2ZTM3NGNhN2QyZDM1YWI1NjRlNzhhMjAwOTMyMjg1MTM5NiJ9; tvonenews_session=eyJpdiI6IkhpQVlNcEtJUUtodHFhcWlaVlhiQkE9PSIsInZhbHVlIjoieUNKdFZEMGhSMUFRZE9QYysweXBhd0ZwdjEzd00xcEdTdHlONk9nUTN6anhyYkNZMlBDQjFHVzdLYWRHdW9YbiIsIm1hYyI6ImNjNThiN2I3NzMwZDBiMzFiZDg1NWM4Njg0MWE2MmU1ODFjODg2ODJmMzdlYmM4ZWYyMzQyOTc3YmU5NWQyM2MifQ%3D%3D; _clsk=z3kyd1%7C1706750342602%7C5%7C1%7Cx.clarity.ms%2Fcollect; FCNEC=%5B%5B%22AKsRol_vmBM14TOCSPFiILrI9jhaV03mAFL1HCGBP-kUDx6mZMQhASFaGaTmEQNDpcF5uu1vlapZWjBvSlIvcncO2CbDGQbvipXfk-k7rSn3dgyFaET_3OlE0vyorgWMb9MBnRrjhJ8pOXNICGapL_OMVfH_cU6sYQ%3D%3D%22%5D%5D")
                        .addHeader("origin", "https://www.tvonenews.com")
                        .addHeader("referer", baseUrl+dateUrl2+"?type=art")
                        .addHeader("sec-ch-ua", "\"Not A(Brand\";v=\"99\", \"Google Chrome\";v=\"121\", \"Chromium\";v=\"121\"")
                        .addHeader("sec-ch-ua-mobile", "?0")
                        .addHeader("sec-ch-ua-platform", "\"Linux\"")
                        .addHeader("sec-fetch-dest", "empty")
                        .addHeader("sec-fetch-mode", "cors")
                        .addHeader("sec-fetch-site", "same-origin")
                        .addHeader("user-agent", userAgent)
                        .addHeader("x-csrf-token", "77618PRwzV9cJ5KCdhlWXiEYgjo9U9UqWv1qfjLM")
                        .addHeader("x-requested-with", "XMLHttpRequest")
                        .build();

                try {
                    Response response = client.newCall(request).execute();
                    Document document = Jsoup.parse(response.body().string());
                    Elements elements = document.select("div.article-list-row div.article-list-info.content_center span a.ali-title");
                    for(Element linkElement : elements) {
                        String href = linkElement.attr("href");
                        if(href.length()!=0 && depth <= 4) {
                            JSONObject jsonUrl = createJsonKafkaUrl(href, baseUrl+dateUrl1+"?type=art", domain, domain, depth, "div.article-list-container > div > div.article-list-info.content_center a.ali-title", "");
                            kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
            }

            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    tvonenewsScrapper(newLandingUrl, newUrl, newDepth);
                }
            }  

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping tvonenews.com");
        }
    }

    public void tvonenewsScrapper(String landingUrl, String url, int depth) throws Exception {
        try {
            String domain = "tvonenews.com";
            depth += 1;
            String topic = "news-topic-tvonenews";
            System.out.println("Processing tvonenews.com: Parsing Content and Related News");

            Document document = Jsoup.connect(url+"?page=all").userAgent(userAgent).get();
            Elements elements = document.select("div.detail-content");
            Elements paragraphs = elements.select("p:not(:contains(Baca Juga))");

            String content = "";
            for(Element paragraph : paragraphs) {
                content += paragraph.text();
            }

            JSONObject jsonContent = createJsonKafkaContent(url, domain, content);

            if(content.length()!=0 && depth <= 4) {
                kafkaService.sendContentToKafka(jsonContent.toString());
            }

            // Parsing link inside content
            Elements linkElements = document.select("div.rbj-list div.rbj-row a");
            for (Element linkNews : linkElements) {
                String href = linkNews.attr("href");
                if(href.length()!=0 && depth <= 4) {
                    JSONObject jsonUrl = createJsonKafkaUrl(href, url, domain, domain, depth, "div.rbj-list div.rbj-row a", "");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }

            }


            // Get list of JSON Url from Kafka
            List<String> jsonFromKafka = kafkaService.parsingKafka(kafkaService.subscribeFromKafka(topic));

            // Iterate list of url to get content
            for (int i = 0; i < jsonFromKafka.size(); i++) {
                // JsonObject json = new JsonParser().parse(jsonFromKafka.get(i)).getAsJsonObject();

                JsonElement jsonElement = JsonParser.parseString(jsonFromKafka.get(i));
                JsonObject json = jsonElement.getAsJsonObject();

                String newUrl = json.get("url").getAsString();
                int newDepth = json.get("depth").getAsInt();
                String newLandingUrl = json.get("landing_url").getAsString();

                if(newDepth <= 4) {
                    tirtoScrapper(newLandingUrl, newUrl, newDepth);
                }
            }  
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to scrapping tvonenews.com");
        }
    }


    // Method to create JSON to send to Kafka 2 (for content)
    public JSONObject createJsonKafkaContent(String url, String domain, String content) {
        JSONObject json = new JSONObject();
        json.put("url", url);
        json.put("domain", domain);
        json.put("content", content);
        json.put("last_checked", new Timestamp(System.currentTimeMillis()).getTime());
        return json;
    }

    // Method to create JSON to send to Kafka 1 (for url)
    public JSONObject createJsonKafkaUrl(String url, String landingUrl, String originalDomain, String domain, int depth, String urlSelect, String contentSelect) {
        JSONObject json = new JSONObject();
        json.put("url", url);
        json.put("landing_url", landingUrl);
        json.put("original_domain", originalDomain);
        json.put("last_checked", LocalDateTime.now());
        json.put("last_checked_ts", new Timestamp(System.currentTimeMillis()).getTime());
        json.put("domain", domain);
        json.put("depth", depth);
        json.put("url_select", urlSelect);
        json.put("content_select", contentSelect);
        json.put("parse_auto", 1);
        return json;
    }

    public JSONObject jsonUrlParser(String jsonString) {

        try {
            ObjectMapper objectMapper = new ObjectMapper();

            // Parse JSON from string
            JsonNode jsonNode = objectMapper.readTree(jsonString);

            JSONObject json = new JSONObject();
            json.put("url", jsonNode.get("url"));
            json.put("depth", jsonNode.get("depth"));
            json.put("landing_url", jsonNode.get("landing_url"));
            json.put("last_checked", jsonNode.get("last_checked"));

            return json;
        } catch (Exception e) {
            throw new JSONException("Faliled to parse JSON");
        }


    }

    public String dateFormatter(String format) {
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);

        String dateString = localDateTime.format(formatter);
        return dateString;
    }

    public String cnnPaginationNews(Elements elementPaging, String content) throws IOException {
        Elements paging = elementPaging.select("div.text-right.my-5");
        if(!paging.isEmpty()) {
            System.out.println("Parsing Pagination of CNN's news");
            String nextPageLink = paging.select("a").attr("href");
            Document docNextPage = Jsoup.connect(nextPageLink).userAgent(userAgent).get();
            Elements elementsNextPage = docNextPage.select("div.detail-text.text-cnn_black.text-sm.grow.min-w-0");
            Elements paragraphsNextPage = elementsNextPage.select("p:not(:contains(Lihat Juga)):not(:contains(ADVERTISEMENT)):not(:contains([Gambas:Video CNN])):not(:contains(SCROLL TO CONTINUE WITH CONTENT)):not(:contains(Baca selengkapnya di sini)):not(:contains(Bersambung ke halaman berikutnya...))");

            for(Element paragraph : paragraphsNextPage) {
                content += paragraph.text();
            }

            content += cnnPaginationNews(elementsNextPage, content);

        }

        return content;
    }

    public void cnnPaginationLink(Elements elementPaging, String url, String domain, int depth, String topic) throws Exception {
        Elements paging = elementPaging.select("div.text-right.my-5");
        if(!paging.isEmpty()) {
            String nextPageLink = paging.select("a").attr("href");
            Document docNextPage = Jsoup.connect(nextPageLink).userAgent(userAgent).get();
            Elements elementsNextPage = docNextPage.select("div.detail-text.text-cnn_black.text-sm.grow.min-w-0");

            // Parsing link inside content
            for (Element link : elementsNextPage.select("div.lihatjg-subtitle a")) {
                String href = link.attr("href");
                if(depth <= 4) {
                    JSONObject jsonUrl = createJsonKafkaUrl(href, url, domain, domain, depth, "div.lihatjg-subtitle a", "");
                    kafkaService.sendUrlToKafka(jsonUrl.toString(), topic);
                }

            }
        }
    }
  
}
