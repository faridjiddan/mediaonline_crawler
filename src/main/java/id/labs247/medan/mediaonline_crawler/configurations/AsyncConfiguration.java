package id.labs247.medan.mediaonline_crawler.configurations;

import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class AsyncConfiguration {

    @Bean(name = "asyncTaskExecutor")
    public ThreadPoolTaskExecutor asyncTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(12); // Set the initial number of threads in the pool
        executor.setMaxPoolSize(15); // Set the maximum number of threads in the pool
        executor.setQueueCapacity(25); // Set the queue capacity for holding pending tasks
        executor.setThreadNamePrefix("AsyncTask-"); // Set a prefix for thread names
        executor.initialize();
        // Set the RejectedExecutionHandler to log an error message
        executor.setRejectedExecutionHandler((Runnable r, ThreadPoolExecutor e) -> {
            // Log the error message indicating the task has been rejected due to the full queue
            // You can customize this message based on your requirements
            System.out.println("Task Rejected: Thread pool is full. Increase the thread pool size.");
        });

        executor.initialize();
        return executor;
    }
}
