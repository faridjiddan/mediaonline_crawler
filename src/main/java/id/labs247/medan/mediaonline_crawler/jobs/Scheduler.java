package id.labs247.medan.mediaonline_crawler.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import id.labs247.medan.mediaonline_crawler.services.NewsScrappingService;

@Component
public class Scheduler {
    
    @Autowired
    private NewsScrappingService newsScrappingService;

    @Async("asyncTaskExecutor")
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void executeKompas() throws Exception {
        newsScrappingService.kompasIndex();
    }

    @Async("asyncTaskExecutor")
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void executeLiputan6() throws Exception {
        newsScrappingService.liputan6Index();
    }

    @Async("asyncTaskExecutor")
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void executeDetik() throws Exception {
        newsScrappingService.detikIndex();
    }

    @Async("asyncTaskExecutor")
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void executeTribunnews() throws Exception {
        newsScrappingService.tribunnewsIndex();
    }

    @Async("asyncTaskExecutor")
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void executeCNN() throws Exception {
        newsScrappingService.cnnIndonesiaIndex();
    }

    @Async("asyncTaskExecutor")
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void executeMediaIndonesia() throws Exception {
        newsScrappingService.mediaIndonesiaIndex();
    }

    @Async("asyncTaskExecutor")
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void executeKumparan() throws Exception {
        newsScrappingService.kumparanIndex();
    }

    @Async("asyncTaskExecutor")
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void executeTempo() throws Exception {
        newsScrappingService.tempoIndex();
    }

    @Async("asyncTaskExecutor")
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void executeBeritasatu() throws Exception { 
        newsScrappingService.beritasatuIndex();
    }

    @Async("asyncTaskExecutor")
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void executeOkezone() throws Exception {
        newsScrappingService.okezoneIndex();
    }

    @Async("asyncTaskExecutor")
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void executeTirto() throws Exception {
        newsScrappingService.tirtoIndex();
    }

    @Async("asyncTaskExecutor")
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void executeTvonenews() throws Exception {
        newsScrappingService.tvonenewsIndex();
    }

    // @Scheduled(cron = "0 * * * * *")
    // public void execute() throws Exception {
    //     newsScrappingService.beritasatuIndex();
    //     newsScrappingService.cnnIndonesiaIndex();
    //     newsScrappingService.detikIndex();
    //     newsScrappingService.kompasIndex();
    //     newsScrappingService.kumparanIndex();
    //     newsScrappingService.liputan6Index();
    //     newsScrappingService.mediaIndonesiaIndex();
    //     newsScrappingService.okezoneIndex();
    //     newsScrappingService.tempoIndex();
    //     newsScrappingService.tirtoIndex();
    //     newsScrappingService.tribunnewsIndex();
    //     newsScrappingService.tvonenewsIndex();
    // }

}
