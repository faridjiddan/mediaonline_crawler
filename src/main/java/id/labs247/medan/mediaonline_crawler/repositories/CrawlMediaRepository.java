package id.labs247.medan.mediaonline_crawler.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.labs247.medan.mediaonline_crawler.entities.CrawlMedia;

@Repository
public interface CrawlMediaRepository extends JpaRepository<CrawlMedia, Long> {
    
    @Query(value = "SELECT * FROM crawl_media WHERE original_domain =?1", nativeQuery= true)
    CrawlMedia getByDomain(String domain);
}
